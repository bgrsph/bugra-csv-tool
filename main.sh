#!/bin/bash
# Written by: Bugra Sipahioglu
# A shell script to extract some information from different files, then collect the information into a csv file
# Last updated on: September/20/2019
# -----------------------------------------------------------

# $1: Path to folder like "15_5_3_24_gcc".

# Checks if a given directory exists. If not, exists the code.
isDirectory() {
  [ ! -d "$1" ] && {
    echo "Directory $1  does not exist."
    exit 1
  }
}

# Extract the thread number from the current folder name
extractThreadNum() {
  dirname="$1"
  result="${dirname%"${dirname##*[!/]}"}"
  result="${result##*/}"
  IFS='_' read -r -a array <<<"$dirname"
  return "${array[-2]}"
}

extractInfoAndPasteToCSV() {
  fileName=$1
  matrixID=$(echo -e "$fileName" | awk -F '.' '{ print $1 }')
  ln=$(echo $(grep -m 1 -Rn "L: (" $fileName | awk '{print $7}'))
  lnnz=$(echo $(grep -m 1 -Rn "L: (" $fileName | awk '{print $11}'))
  analysisTime=$(grep -m 1 -Rn "$fileName" analysisTimes | awk -F ' ' ' { print $7 } ')
  execTime=$(grep -m 1 -Rn "$fileName" execTimes | awk -F ' ' ' { print $5 } ')
  statsFileName=$(echo -e "$(echo -e "$fileName" | awk -F '.' '{ print $1 }').stats.csv")

  cd log
  targetLineNum=$(($(grep -m 1 -Rn "avgTotalDD" $statsFileName | awk -F ':' '{ print $1 }') + 1))
  workLoadGran=$(sed " "$targetLineNum"q;d" $statsFileName | awk -F ',' '{ print $1 }') 
  avgTotalDD=$(sed " "$targetLineNum"q;d" $statsFileName | awk -F ',' '{ print $2 }')
  maxTotalDDLevel=$(sed " "$targetLineNum"q;d" $statsFileName | awk -F ',' '{ print $3 }')
  #maxTotalDD=$(sed " "$targetLineNum"q;d" $statsFileName | awk -F ',' '{ print $4 }')
  avg_rows=$(sed " "$targetLineNum"q;d" $statsFileName | awk -F ',' '{ print $4 }')
  avg_nnz=$(sed " "$targetLineNum"q;d" $statsFileName | awk -F ',' '{ print $5 }')
  avg_child_ptg_level=$(sed " "$targetLineNum"q;d" $statsFileName | awk -F ',' '{ print $6 }')
  avg_child_ptg_thread=$(sed " "$targetLineNum"q;d" $statsFileName | awk -F ',' '{ print $7 }')
  num_rows_in_one_remaining=$(sed " "$targetLineNum"q;d" $statsFileName | awk -F ',' '{ print $8 }')
  num_levels=$(grep -m 1 -Rn "NUM_LEVELS" $statsFileName | awk -F ',' '{ print $2 }')
  #stdev=$(grep -m 1 -Rn "stdev" $statsFileName | awk -F ',' '{ print $2 }')
  #avgTotalDDThread=$(grep -m 1 -Rn "avgTotalDDThread" $statsFileName | awk -F ',' '{ print $2 }')
  #maxDD=$(grep -m 1 -Rn "maxDD" $statsFileName | awk -F ',' '{ print $2 }')
  stdev_threads=$(grep -m 1 -Rn "stdev threads" $statsFileName | awk -F ',' '{ print $2 }')
  stdev_comm_costs=$(grep -m 1 -Rn "stdev comm costs" $statsFileName | awk -F ',' '{ print $2 }')
  cd ..
  echo -e "$matrixID,$ln,$lnnz,$avg_rows,$avg_nnz,$avg_child_ptg_level,$avg_child_ptg_thread,$num_rows_in_one_remaining,$workLoadGran,$avgTotalDD,$maxTotalDDLevel,$stdev_threads,$stdev_comm_costs,$analysisTime,$execTime" | paste -sd ',' >>$out_file_name
}

#Path to folder 15_5_3_24_gcc
path=$1

# Name of the output csv file
out_file_name="metrics.csv"

# If wrong path is given, rise an error and exit
isDirectory $path

# Go to given path
cd "$path"

# remove the old metrics.csv to prevent appending to it
if test -f "$out_file_name"; then rm $out_file_name; fi

# Get the thread number from the path
extractThreadNum $path
thread_num=$?

# Column titles for the csv file
echo -e "matrixID,ln,lnnz,avg # of rows per level,avg # of nnzs per level,avg. of childrenPtgLevel,avg. of childrenPtgThread,num. of rows in oneRemaining,workLoadGranularity,avgTotalDD,maxTotalDDLevel,stdev threads,stdev comm costs,analysis time,exec time" | paste -sd ',' >>$out_file_name

# Iterate over the files in the given path, then construct the csv file.
find $path/* -maxdepth 0 -iname "*.$thread_num" -type f -prune -printf "%f\n" | while read -r matrixFile; do
  extractInfoAndPasteToCSV "$matrixFile"
done

exit 0
